import { observable, flow } from 'mobx';

import productAPI from '../apis/productAPI';
import cloudinaryAPI from '../apis/cloudinaryAPI';
import { Product } from '../types/Product-type';

export class ProductStore {
  @observable products: Product[] = [];
  @observable isFetching = false;
  @observable newImageUrl = '';
  @observable uploadingImage = false;
  @observable isEditSuccessful = false;
  @observable errorMessage = '';

  fetchProducts = flow(function* (this: ProductStore) {
    this.products = [];
    this.isFetching = true;

    try {
      const response = yield productAPI.getProducts();
      console.log(response);
      this.isFetching = false;
      this.products = response.data;
    } catch (error) {
      this.isFetching = false;
    }
  });

  uploadImage = flow(function* (this: ProductStore, file: File) {
    this.newImageUrl = '';
    this.uploadingImage = true;
    try {
      const response = yield cloudinaryAPI.uploadImage(file);
      this.newImageUrl = response.secure_url;
      this.uploadingImage = false;
    } catch (error) {
      this.uploadingImage = false;
      console.log(error);
    }
  });

  editProduct = flow(function* (
    this: ProductStore,
    data: {
      productNo: string;
      productName: string;
      price: number;
      desc: string;
      image: string;
    },
  ) {
    this.isFetching = true;
    this.errorMessage = '';
    try {
      const { productNo, ...otherData } = data;
      const response = yield productAPI.editProduct(productNo, otherData);
      console.log(response);
      this.isFetching = false;
      this.isEditSuccessful = true;
    } catch (error) {
      this.isFetching = false;
      console.log(error);
      this.errorMessage = 'Failed to edit product';
    }
  });

  deleteProduct = flow(function* (this: ProductStore, productNo: string) {
    try {
      const response = yield productAPI.deleteProduct(productNo);
      console.log(response);
      this.fetchProducts();
    } catch (error) {
      this.isFetching = false;
      console.log(error);
    }
  });
}

export const productStore = new ProductStore();
