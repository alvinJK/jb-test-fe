import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { ProductListScene, ProductDetailScene } from './scenes';

export default class Router extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact key={'productList'} path={'/'} component={ProductListScene} />
          <Route exact key={'productDetail'} path={'/product/:productNo'} component={ProductDetailScene} />
          <Route
            render={() => {
              return <div>{'404 - Not Found'}</div>;
            }}
          />
        </Switch>
      </BrowserRouter>
    );
  }
}
