import fetcher from './fetcher';
import { AxiosInstance, AxiosRequestConfig } from 'axios';

export function createProductAPI(fetch: AxiosInstance) {
  const productAPI = {
    async getProducts() {
      const options: AxiosRequestConfig = {
        method: 'GET',
      };
      const result = await fetch('/products', options);
      return result.data;
    },
    async getProductDetail(productNo: string) {
      const options: AxiosRequestConfig = {
        method: 'GET',
      };
      const result = await fetch(`/products/${productNo}`, options);
      return result.data;
    },
    async editProduct(
      productNo: string,
      data: {
        productName: string;
        desc: string;
        price: number;
        image: string;
      },
    ) {
      const options: AxiosRequestConfig = {
        method: 'PATCH',
        data,
      };
      const result = await fetch(`/products/${productNo}`, options);
      return result.data;
    },
    async deleteProduct(productNo: string) {
      const options: AxiosRequestConfig = {
        method: 'DELETE',
      };
      const result = await fetch(`/products/${productNo}`, options);
      return result.data;
    },
  };
  return productAPI;
}

export default createProductAPI(fetcher);
