import Axios from 'axios';
import { AxiosRequestConfig } from 'axios';

const fetcher = Axios.create({
  baseURL: 'https://api.cloudinary.com/v1_1/houzhunter',
});

const cloudinaryAPI = {
  async uploadImage(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', 'image-developer');

    const options: AxiosRequestConfig = {
      method: 'POST',
      data: formData,
    };
    const result = await fetcher('/auto/upload', options);
    return result.data;
  },
};

export default cloudinaryAPI;
