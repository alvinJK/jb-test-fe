import Axios from 'axios';

const fetcher = Axios.create({
  baseURL: 'http://localhost:3030',
});

export default fetcher;
