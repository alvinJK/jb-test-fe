import React from 'react';
import { Provider } from 'mobx-react';

import { productStore } from './stores/ProductStore';
import Router from './Router';

function App() {
  return (
    <Provider productStore={productStore}>
      <Router />
    </Provider>
  );
}

export default App;
