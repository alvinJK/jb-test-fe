import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect, RouteComponentProps, Link } from 'react-router-dom';
import Dropzone from 'react-dropzone';

import { ProductStore } from '../stores/ProductStore';
import productAPI from '../apis/productAPI';

type RouteParams = { productNo: string };
type Props = RouteComponentProps<RouteParams> & {
  productStore: ProductStore;
};
type State = {
  isLoadingData: boolean;
  productName: string;
  desc: string;
  price: number;
  image: string;
};

@inject('productStore')
@observer
export default class ProductDetailScene extends Component<Props, State> {
  state: State = {
    isLoadingData: false,
    productName: '',
    desc: '',
    price: 0,
    image: '',
  };
  componentDidMount() {
    const { productNo } = this.props.match.params;
    this._fetchProductDetail(productNo);
  }
  render() {
    const { isLoadingData, productName, price, desc } = this.state;
    if (isLoadingData) {
      return <div style={styles.rootContainer}>LOADING</div>;
    }
    const { isEditSuccessful, errorMessage, isFetching } = this.props.productStore;
    if (isEditSuccessful) {
      return <Redirect to="/" />;
    }
    return (
      <div style={styles.rootContainer}>
        <div style={styles.row}>
          <span style={styles.label}>Name</span>
          <input
            type="text"
            style={styles.value}
            value={productName}
            onChange={(e) => {
              this.setState({ productName: e.currentTarget.value });
            }}
          />
        </div>
        <div style={styles.row}>
          <span style={styles.label}>Price</span>
          <input
            type="number"
            style={styles.value}
            value={price}
            onChange={(e) => {
              this.setState({ price: Number(e.currentTarget.value) });
            }}
          />
        </div>
        <div style={styles.row}>
          <span style={styles.label}>Description</span>
          <textarea
            style={styles.textAreaValue}
            value={desc}
            onChange={(e) => {
              this.setState({ desc: e.currentTarget.value });
            }}
          />
        </div>
        <div style={styles.row}>
          <span style={styles.label}>Image</span>
          {this._renderDropzone()}
        </div>
        <div style={styles.errorMessageContainer}>{errorMessage}</div>
        <div style={styles.row}>
          <div style={styles.label}></div>
          <div style={styles.value}>
            <Link to="/" style={styles.cancelButton}>
              {'CANCEL'}
            </Link>
            <button style={styles.submitButton} onClick={this._onSubmit}>
              {isFetching ? 'Submitting...' : 'SUBMIT'}
            </button>
          </div>
        </div>
      </div>
    );
  }
  _fetchProductDetail = async (productNo: string) => {
    this.setState({
      isLoadingData: true,
    });
    const { data: productDetail } = await productAPI.getProductDetail(productNo);
    const { productName, desc, price, image } = productDetail;
    this.setState({
      isLoadingData: false,
      productName,
      desc,
      price,
      image,
    });
  };

  _renderDropzone = () => {
    const { newImageUrl, uploadingImage } = this.props.productStore;
    const { image } = this.state;
    return (
      <Dropzone onDrop={this._onImageDrop} multiple={false}>
        {({ getRootProps, getInputProps }) => (
          <div style={styles.dropzoneContainer} {...getRootProps()}>
            <input {...getInputProps()} />
            {uploadingImage ? (
              <div>UPLOADING, PLEASE WAIT</div>
            ) : image || newImageUrl ? (
              <img style={styles.image} src={newImageUrl || image} alt="product" />
            ) : null}
            <span style={styles.dropzoneSubtitle}>Drag an image here, or click to select file</span>
          </div>
        )}
      </Dropzone>
    );
  };

  _onImageDrop = (file: Array<File>) => {
    this.props.productStore.uploadImage(file[0]);
  };

  _onSubmit = () => {
    const { productNo } = this.props.match.params;
    const { productName, desc, price, image } = this.state;
    const { isFetching, newImageUrl } = this.props.productStore;
    if (isFetching) {
      return;
    }
    this.props.productStore.editProduct({ productNo, productName, desc, price, image: newImageUrl || image });
  };
}

const styles: { [key: string]: React.CSSProperties } = {
  rootContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: 1280,
    padding: 20,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    margin: '10px 0',
  },
  label: {
    width: 120,
    fontSize: 16,
    fontWeight: 600,
    textAlign: 'right',
    paddingTop: 12,
    paddingRight: 10,
  },
  value: {
    width: 300,
    padding: 10,
    fontSize: 14,
    display: 'flex',
    flexDirection: 'row',
  },
  textAreaValue: {
    width: 300,
    height: 120,
    padding: 10,
    fontSize: 14,
    resize: 'none',
  },
  dropzoneContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 300,
    height: 300,
    padding: 10,
    border: 'solid 1px #ccc',
    borderRadius: '1rem',
  },
  image: {
    maxWidth: 280,
    maxHeight: 250,
  },
  dropzoneSubtitle: {
    fontSize: 10,
  },
  cancelButton: {
    padding: '10px 20px',
    backgroundColor: '#212121',
    color: '#FFF',
    fontWeight: 700,
    textAlign: 'center',
    fontSize: 12,
    textDecoration: 'none',
    flex: 1,
    marginRight: 10,
  },
  submitButton: {
    padding: '10px 20px',
    backgroundColor: '#0C2C5E',
    color: '#FFF',
    fontWeight: 700,
    textAlign: 'center',
    flex: 1,
  },
  errorMessageContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0',
    color: '#F00',
  },
};
