import React from 'react';
import { observer, inject } from 'mobx-react';

import { ProductStore } from '../stores/ProductStore';
import { ProductCard } from '../components';

type Props = {
  productStore: ProductStore;
};

@inject('productStore')
@observer
export default class ProductListScene extends React.Component<Props> {
  componentDidMount() {
    const { products, isFetching, isEditSuccessful } = this.props.productStore;
    if (isEditSuccessful || (products.length === 0 && !isFetching)) {
      this.props.productStore.fetchProducts();
    }
  }
  render() {
    const { products, isFetching } = this.props.productStore;

    if (isFetching) {
      return <div style={styles.rootContainer}>LOADING</div>;
    }
    return (
      <div style={styles.rootContainer}>
        {products.map((product) => (
          <ProductCard
            key={product.productNo}
            product={product}
            onDelete={() => {
              this.props.productStore.deleteProduct(String(product.productNo));
            }}
          />
        ))}
      </div>
    );
  }
}

const styles: { [key: string]: React.CSSProperties } = {
  rootContainer: {
    display: 'grid',
    gridColumnGap: 20,
    gridRowGap: 20,
    gridTemplateColumns: 'repeat(auto-fit, minmax(300px, 1fr))',
    width: 1280,
    padding: 20,
  },
};
