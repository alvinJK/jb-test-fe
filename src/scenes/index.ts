import ProductDetailScene from './ProductDetailScene';
import ProductListScene from './ProductListScene';

export { ProductDetailScene, ProductListScene };
