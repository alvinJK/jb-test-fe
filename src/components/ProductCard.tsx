import React from 'react';

import { Product } from '../types/Product-type';
import { Link } from 'react-router-dom';

type Props = { product: Product; onDelete: () => void };

const MAX_DESC_LENGTH = 120;
const MAX_NAME_LENGTH = 60;

export default function ProductCard(props: Props) {
  const { onDelete } = props;
  const { price, image, productNo } = props.product;
  let { desc, productName } = props.product;
  if (desc.length > MAX_DESC_LENGTH) {
    desc = `${desc.substr(0, MAX_DESC_LENGTH)}...`;
  }
  if (productName.length > MAX_NAME_LENGTH) {
    productName = `${productName.substr(0, MAX_NAME_LENGTH)}...`;
  }

  return (
    <div style={styles.rootContainer}>
      <div style={styles.photoContainer}>
        <img style={styles.productPhoto} src={image} alt={productName} />
      </div>
      <div style={styles.header}>
        <span style={styles.productName}>{productName}</span>
        <span style={styles.productPrice}>{`Rp ${formatNumber(price)}`}</span>
      </div>
      <div style={styles.body}>{desc}</div>
      <div style={styles.buttonContainer}>
        <Link to={`/product/${productNo}`} style={styles.editButton}>
          Edit
        </Link>
        <div style={styles.deleteButton} onClick={onDelete}>
          Delete
        </div>
      </div>
    </div>
  );
}

function formatNumber(num: number) {
  return num
    .toFixed(2)
    .replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
    .split('.')[0]
    .replace(/,/g, '.');
}

const styles: { [key: string]: React.CSSProperties } = {
  rootContainer: {
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    justifyContent: 'center',
    border: 'solid 1px #ccc',
    borderRadius: '1rem',
  },
  photoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 150,
    backgroundColor: '#ddd',
    borderTopLeftRadius: '1rem',
    borderTopRightRadius: '1rem',
  },
  productPhoto: {
    maxHeight: 150,
    maxWidth: 300,
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    borderTop: 'solid 1px #ccc',
    padding: '10px 20px',
    height: 60,
  },
  productName: {
    fontSize: 16,
    fontWeight: 500,
    marginBottom: 10,
    flex: 1,
  },
  productPrice: {
    fontSize: 12,
    fontWeight: 700,
    color: '#f00',
  },
  body: {
    padding: '10px 20px',
    height: 60,
    fontSize: 12,
  },
  buttonContainer: {
    position: 'absolute',
    top: 12,
    right: 10,
    fontSize: 10,
  },
  editButton: {
    display: 'inline-block',
    backgroundColor: '#fff',
    textDecoration: 'none',
    color: '#000',
    padding: 8,
    borderRadius: '0.5rem',
    marginRight: 6,
  },
  deleteButton: {
    display: 'inline-block',
    backgroundColor: '#fff',
    textDecoration: 'none',
    color: '#f00',
    padding: 8,
    borderRadius: '0.5rem',
  },
};
