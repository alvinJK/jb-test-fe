export type Product = {
  productNo: number;
  productName: string;
  desc: string;
  price: number;
  image: string;
};
